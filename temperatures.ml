(* 
Beyond the well-known Celsius and Fahrenheit, there are other six 
temperature scales: Kelvin, Rankine, Delisle, Newton, Réaumur, and Rømer

1. Write a function that given a pure number prints a conversion table for 
it among any of the 8 scales (remember that functions are high-order).
*)
type scale = C | F | K | R | De | N | Re | Ro

let string_of_scale = function
  | C -> "Celsius"
  | F -> "Fahrenheit"
  | K -> "Kelvin"
  | R -> "Rankine"
  | De -> "Delisle"
  | N -> "Newton"
  | Re -> "Réaumur"
  | Ro -> "Rømer"

let from_celsius value = function
  | C -> value
  | F -> (value *. 9. /. 5.) +. 32.
  | K -> value +. 273.15
  | R -> (value +. 273.15) *. 9. /. 5.
  | De -> (100. -. value) *. 3. /. 2.
  | N -> value *. 33. /. 100.
  | Re -> value *. 4. /. 5.
  | Ro -> (value *. 21. /. 40.) +. 7.5

let to_celsius value = function
  | C -> value
  | F -> (value -. 32.) *. 5. /. 9.
  | K -> value -. 273.15
  | R -> (value -. 491.67) *. 5. /. 9.
  | De -> 100. -. (value *. 2. /. 3.)
  | N -> value *. 100. /. 33.
  | Re -> value *. 5. /. 4.
  | Ro -> (value -. 7.5) *. 40. /. 21.

let print_converted_scales value =
  let rec print_other_scales = function
    | [] -> ()
    | x :: tl ->
        let string_temp = string_of_float (from_celsius value x) in
        let string_scale = string_of_scale x in
        print_endline (string_temp ^ " " ^ string_scale) ;
        print_other_scales tl in
  print_other_scales [C; F; K; R; De; N; Re; Ro]

print_converted_scales 0.
print_converted_scales 30.

(* 
2. Write a function that given a temperature in a specified scale returns a list
 of all the corresponding temperatures in the other scales, note that the scale
  must be specified (hint: use a tuple).
*)

let from_scale_to_all value scale =
  let scales = [C; F; K; R; De; N; Re; Ro] in
  List.init 8 (fun x ->
      ( from_celsius (to_celsius value scale) (List.nth scales x)
      , List.nth scales x ) )

from_scale_to_all 150. De
from_scale_to_all 45. Ro
from_scale_to_all 12. K
