(*
Let's write a function (or a pool of functions) that given a quite large text
(over than 2000 words) counts how frequent each word occurs in the text.

The text is read from a file (look at the pervasive module in the manual) and 
it is a real text with punctuation (i.e., commas, semicolons, ...) that should
 be counted.
*)
#load "str.cma"

let read_file filename =
  let eval_split_result x = match x with Str.Text t -> t | Str.Delim d -> d in
  let lines = ref [] and chan = open_in filename in
  try
    while true do
      let uppercase_line = String.uppercase_ascii (input_line chan) in
      let splitted_line =
        Str.full_split (Str.regexp "[^a-zA-Z]") uppercase_line in
      let transformed_line =
        List.map (fun x -> eval_split_result x) splitted_line in
      lines := transformed_line :: !lines
    done ;
    !lines
  with End_of_file -> close_in chan ; List.rev !lines

let rec compact_lines ?(acc = []) = function
  | [] -> acc
  | x :: tl -> compact_lines ~acc:(acc @ x) tl

let scan_file filename =
  let rec scan table sorted_words =
    match sorted_words with
    | [] -> List.rev table
    | x :: _ ->
        let grouped_x, others = List.partition (fun w -> w = x) sorted_words in
        scan ((x, List.length grouped_x) :: table) others in
  scan [] (List.sort compare (compact_lines (read_file filename)))

let rec print_table = function
  | [] -> Printf.printf "\n"
  | (x, y) :: xs ->
      Printf.printf "%s - %d\n" x y ;
      print_table xs

print_table (scan_file "gems.txt")
