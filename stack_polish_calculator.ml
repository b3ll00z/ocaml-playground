(*
Write a PolishCalculator module that implements a stack-based calculator that 
adopts polish notation for the expressions to be evaluated.

Polish Notation is a prefix notation where in every operator follows all of its 
operands; this notation has the big advantage of being unambiguous and permits 
to avoid the use of parenthesis. E.g., (3+4)*5 is equal to 3 4 + 5 *.

The module should include an:

    + Expr datatype representing an expression
    
    + a function expr_of_string: string → Expr which build the expression in the 
    		corresponding infix notation out of the string in polish notation;
    
    + a function eval: Expr → int which will evaluate the expression and returns
     	such evaluation

The evaluation/translation can be realized by pushing the recognized elements on
a stack. Define the module independently of the Stack implementation and try to
use functors to adapt it.
*)

module type Stack = sig
  type 'a stack

  exception EmptyStackException

  val empty : unit -> 'a stack
  val is_empty : 'a stack -> bool
  val push : 'a -> 'a stack -> unit
  val pop : 'a stack -> 'a
end

module StackImpl : Stack = struct
  type 'a stack = {mutable c: 'a list}

  exception EmptyStackException

  let empty () = {c= []}
  let is_empty s = s.c = []
  let push x s = s.c <- x :: s.c

  let pop s =
    match s.c with
    | [] -> raise EmptyStackException
    | x :: tl ->
        s.c <- tl ;
        x
end

#load "str.cma"

module PolishCalculator (S : Stack) = struct
  type expr =
    | Int of int
    | Plus of expr * expr
    | Sub of expr * expr
    | Times of expr * expr
    | Div of expr * expr
    | Pow of expr * expr

  let expr_of_string postfix_string =
    let postfix_list = String.split_on_char ' ' postfix_string in
    let is_number s = Str.string_match (Str.regexp "^[0-9]+$") s 0 in
    let rec to_expr stack = function
      | [] ->
          if S.is_empty stack then raise (Failure "Something went wrong")
          else S.pop stack
      | x :: xs when is_number x ->
          S.push (Int (int_of_string x)) stack ;
          to_expr stack xs
      | x :: xs -> (
          let e_dx = S.pop stack in
          try
            let e_sx = S.pop stack in
            match x with
            | "+" ->
                S.push (Plus (e_sx, e_dx)) stack ;
                to_expr stack xs
            | "-" ->
                S.push (Sub (e_sx, e_dx)) stack ;
                to_expr stack xs
            | "*" ->
                S.push (Times (e_sx, e_dx)) stack ;
                to_expr stack xs
            | "/" ->
                S.push (Div (e_sx, e_dx)) stack ;
                to_expr stack xs
            | "**" ->
                S.push (Pow (e_sx, e_dx)) stack ;
                to_expr stack xs
            | _ -> raise (Failure "ciao")
          with S.EmptyStackException -> (
            match x with
            | "-" ->
                S.push (Sub (Int 0, e_dx)) stack ;
                to_expr stack xs
            | _ -> raise (Failure "Expression not correct") ) ) in
    to_expr (S.empty ()) postfix_list

  let rec eval = function
    | Int n -> n
    | Plus (e_sx, e_dx) -> eval e_sx + eval e_dx
    | Sub (e_sx, e_dx) -> eval e_sx - eval e_dx
    | Times (e_sx, e_dx) -> eval e_sx * eval e_dx
    | Div (e_sx, e_dx) -> eval e_sx / eval e_dx
    | Pow (e_sx, e_dx) -> int_of_float (float (eval e_sx) ** float (eval e_dx))
end

module RPNCalculator = PolishCalculator (StackImpl);;

RPNCalculator.expr_of_string "11 5 - 1 -"
RPNCalculator.expr_of_string "3 - *"

let some_postfix_expressions =
  [ ("4 5 7 2 + - *", -16); ("3 4 + 2 * 7 /", 2); ("5 7 + 6 2 - *", 48)
  ; ("4 2 3 5 1 - + * +", 18); ("4 2 + 3 5 1 - * +", 18)
  ; ("2 5 ** 3 - - 2 +", -27); ("11 22 + 33 +", 66)
  ; ("30 400 * 15 60 * +", 12900); ("10 2 8 * + 3 -", 23) ]

List.for_all
  (fun (x, y) -> RPNCalculator.eval (RPNCalculator.expr_of_string x) == y)
  some_postfix_expressions
