(* Write the matrix datatype with the following operations: *)

type matrix = int array array

(* A function zeroes to construct a matrix of size n×m filled with zeros *)

let zeroes n m : matrix = Array.make_matrix n m 0

zeroes 3 4
zeroes 1 3

(* A function identity to construct the identity matrix (the one with all 0s
 but the 1s on the diagonal) of given size *)

let identity n : matrix =
  Array.init n (fun x -> Array.init n (fun y -> if x = y then 1 else 0))

identity 1
identity 4
identity 6

(* A function init to construct a square matrix of a given size n filled 
with the first n×n integers. *)

let init n : matrix =
  Array.init n (fun x -> Array.init n (fun y -> (x * n) + y + 1))

init 3
init 5

(* A function transpose that transposes a generic matrix independently of
 its size and content. *)

let transpose mm : matrix =
  let n = Array.length mm in
  let m = Array.length mm.(0) in
  Array.init m (fun x -> Array.init n (fun y -> mm.(y).(x)))

transpose (init 3)
transpose [|[|4; 3; 1|]; [|2; 5; 6|]; [|6; 1; 0|]; [|2; 7; 8|]|]

(* A function * that multiplies two matrices non necessarily square matrices. *)

let ( * ) (x : matrix) (y : matrix) : matrix =
  let n = Array.length x in
  let m = Array.length y.(0) in
  let local_products w z =
    Array.init (Array.length w) (fun x -> w.(x) * z.(x)) in
  Array.init n (fun i ->
      Array.init m (fun j ->
          Array.fold_left ( + ) 0 (local_products x.(i) (transpose y).(j)) ) )

[|[|1; 0; 1|]; [|1; 5; -1|]; [|3; 2; 0|]|] * [|[|7; 1|]; [|1; 0|]; [|0; 4|]|]
[|[|3; 6|]; [|1; 2|]|] * [|[|0; 2|]; [|0; -1|]|]
