(*
	Put into a list, called alkaline_earth_metals, the atomic numbers of the 
	six alkaline earth metals: beryllium (4), magnesium (12), calcium (20), 
	strontium (38), barium (56), and radium (88). Then
	
	1. Write a function that returns the highest atomic number in alkaline_earth_metals.	
*)

let alkaline_earth_metals =
  [ ("beryllium", 4); ("magnesium", 12); ("calcium", 20); ("strontium", 38)
  ; ("barium", 56); ("radium", 88) ]

let max_atomic_number =
  let rec get_max_number metals max =
    match metals with
    | [] -> max
    | (metal, atomic_number) :: other_metals ->
        if atomic_number > max then get_max_number other_metals atomic_number
        else get_max_number other_metals max in
  get_max_number
    (List.tl alkaline_earth_metals)
    (snd (List.hd alkaline_earth_metals))

max_atomic_number

(*
	2. Write a function that sorts alkaline_earth_metals in ascending order 
	(from the lightest to the heaviest).
*)

let sort_atoms op atoms =
  let rec sort_by_op = function
    | [] -> []
    | atom :: others ->
        let minors, majors = List.partition (fun x -> op x < op atom) others in
        sort_by_op minors @ [atom] @ sort_by_op majors in
  sort_by_op atoms

let sort_atoms_by_name = sort_atoms fst
let sort_atoms_by_number = sort_atoms snd

sort_atoms_by_name alkaline_earth_metals
sort_atoms_by_number alkaline_earth_metals

(*
Put into a second list, called noble_gases, the noble gases: helium (2), neon (10)
, argon (18), krypton (36), xenon (54), and radon (86).
*)

let noble_gases =
  [ ("helium", 2); ("neon", 10); ("argon", 18); ("krypton", 36); ("xenon", 54)
  ; ("radon", 86) ]

(*
Write a function (or a group of functions) that merges the two lists and print 
the result as couples (name, atomic number) sorted in ascending order on the element names.
*)

let print_sorted_elements =
  let rec print_tuples = function
    | [] -> print_newline
    | (x, y) :: tail ->
        print_endline (" " ^ x ^ ", " ^ string_of_int y) ;
        print_tuples tail in
  print_tuples (sort_atoms_by_name (alkaline_earth_metals @ noble_gases))

print_sorted_elements
