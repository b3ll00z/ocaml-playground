(*
Goldbach's conjecture is one of the oldest unsolved problems in number theory 
and in all of mathematics. It states:

Every even integer greater than 2 is a Goldbach number, i.e., a number that can
be expressed as the sum of two primes.

Expressing a given even number as a sum of two primes is called a Goldbach 
partition of the number. For example, 4 = 2 + 2, 6 = 3 + 3, 8 = 3 + 5 and so on..

Write the following functions:
    goldbach(n) that returns a Goldbach partition for n
*)

let is_prime n =
  let rec check_prime step =
    if step * step <= n then
      if n mod step <> 0 then check_prime (step + 2) else false
    else true in
  if n < 1 || (n mod 2 = 0 && n <> 2) then false else check_prime 3

let goldbach n =
  let search k =
    let rec find_primes x y =
      if is_prime x && is_prime y && x + y = n then (x, y)
      else find_primes (x - 2) (y + 2) in
    if k mod 2 <> 0 then find_primes (k - 2) (k + 2)
    else find_primes (k - 1) (k + 1) in
  let goldbach_partitions k =
    let half = k / 2 in
    if is_prime half then (half, half) else search half in
  if n mod 2 <> 0 || n <= 2 then
    raise (Failure "The number must be an even greater than 2")
  else goldbach_partitions n

(* 
goldbach_list(n,m) that returns a list of Goldbach partitions for the even numbers in the range (n,m).
*)
let goldbach_list n m =
  let rec make_even ?(acc = []) step =
    if step <= m then make_even ~acc:(acc @ [step]) (step + 2)
    else List.map (fun x -> (x, goldbach x)) acc in
  if n mod 2 <> 0 then make_even (n + 1) else make_even n

(* Usage: $ ocaml < goldbach.ml *)

is_prime 101
is_prime 4219
goldbach 52
goldbach 72
goldbach_list 200 291
