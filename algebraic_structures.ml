(*
To interact: $ ocaml < algebraic_structures.ml

Write the Monoid, Group and Ring modules implementing the monoids, groups and 
rings algebraic structures respectively. Each module must be parametric on the 
algebraic sort, in the sense that the operations and the sets are not a priori 
defined and they should implement operations to check the properties 
characterizing the algebraic structure.

A monoid is an algebraic structure consisting of a set together with a single 
associative binary operation and an identity element. E.g., the set of booleans
with the or operator is a monoid whose identity is false.
*)

module type Operation = sig
  type t

  val set : t list

  val op : t -> t -> t

  val neutral : t
end

module Monoid (Op : Operation) = struct
  let check_identity = List.for_all (fun x -> x = Op.op x Op.neutral) Op.set

  let check_associativity =
    let rec test_associativity lx ly lz =
      match lx with
      | [] ->
          true
      | x :: tx -> (
        match ly with
        | [] ->
            test_associativity tx Op.set lz
        | y :: ty -> (
          match lz with
          | [] ->
              test_associativity lx ty Op.set
          | z :: tz when Op.op x (Op.op y z) = Op.op (Op.op x y) z ->
              test_associativity lx ly tz
          | _ ->
              false ) )
    in
    test_associativity Op.set Op.set Op.set

  let check_closure =
    let rec test_closure lx ly =
      match lx with
      | [] ->
          true
      | x :: tx -> (
        match ly with
        | [] ->
            test_closure tx Op.set
        | y :: ty when List.exists (fun k -> k = Op.op x y) Op.set ->
            test_closure lx ty
        | _ ->
            false )
    in
    test_closure Op.set Op.set
end

(*
A group is an algebraic structure consisting of a set together with an operation
that combines any two of its elements to form a third element. To qualify as a 
group, the set and the operation must satisfy a few conditions called group axioms,
namely closure, associativity, identity and invertibility. 
E.g., the set Z (the integers with sign) with the + operator is a group but with
the * is not since inverting the operation break the closure property.
*)

module Group (Op : Operation) = struct
  include Monoid (Op)

  let check_invertibility =
    let rec test_invertibility = function
      | [] ->
          true
      | x :: tl when List.exists (fun y -> Op.op x y = Op.neutral) Op.set ->
          test_invertibility tl
      | _ ->
          false
    in
    test_invertibility Op.set
end

(*
A ring is an algebraic structure consisting of a set together with two binary 
operations (usually called addition and multiplication), where each operation 
combines two elements to form a third element. To qualify as a ring, the set 
together with its two operations must satisfy certain conditions, namely, the 
set must be an abelian (i.e., commutative) group under addition and a monoid 
under multiplication such that multiplication distributes over addition.
*)
module type RingOperation = sig
  type t

  val set : t list

  val add : t -> t -> t

  val mul : t -> t -> t

  val neutral_add : t

  val neutral_mul : t
end

module Ring (Op : RingOperation) : sig
  val check_monoid_on_mul : bool

  val check_abelian_group_on_add : bool

  val check_distributive : bool
end = struct
  module MonoidChecker = Monoid (struct
    type t = Op.t

    let set = Op.set

    let op = Op.mul

    let neutral = Op.neutral_mul
  end)

  module GroupChecker = Group (struct
    type t = Op.t

    let set = Op.set

    let op = Op.add

    let neutral = Op.neutral_add
  end)

  let check_monoid_on_mul =
    MonoidChecker.check_identity && MonoidChecker.check_associativity
    && MonoidChecker.check_closure

  let check_abelian_group_on_add =
    GroupChecker.check_identity && GroupChecker.check_associativity
    && GroupChecker.check_closure && GroupChecker.check_invertibility

  let check_distribution_on_add a b c =
    Op.mul a (Op.add b c) = Op.add (Op.mul a b) (Op.mul a c)
    && Op.mul (Op.add a b) c = Op.add (Op.mul a c) (Op.mul b c)

  let check_distributive =
    let rec test_distributive lx ly lz =
      match lx with
      | [] ->
          true
      | x :: tx -> (
        match ly with
        | [] ->
            test_distributive tx Op.set lz
        | y :: ty -> (
          match lz with
          | [] ->
              test_distributive lx ty Op.set
          | z :: tz when check_distribution_on_add x y z ->
              test_distributive lx ly tz
          | _ ->
              false ) )
    in
    test_distributive Op.set Op.set Op.set
end

(* NB: all tests below are based on finite sets *)
(* Test monoids *)
module Booleans : Operation = struct
  type t = bool

  let set = [true; false]

  let op = ( || )

  let neutral = false
end

module First4NaturalsAdd : Operation = struct
  type t = int

  let set = [0; 1; 2; 3]

  let op x y = (x + y) mod 4

  let neutral = 0
end

module BooleansMonoidTester = Monoid (Booleans);;

BooleansMonoidTester.check_identity
BooleansMonoidTester.check_associativity
BooleansMonoidTester.check_closure

module First4NaturalMonoidTester = Monoid (First4NaturalsAdd);;

First4NaturalMonoidTester.check_identity
First4NaturalMonoidTester.check_associativity
First4NaturalMonoidTester.check_closure

(* Test groups *)
module First4NaturalsMul : Operation = struct
  type t = int

  let set = [0; 1; 2; 3]

  let op x y = x * y mod 4

  let neutral = 1
end

module MonoalphabeticString : Operation = struct
  type t = string

  let set = [""; "a"; "aaa"; "aaaa"; "aaaaaaa"]

  let op x y = if String.length x > String.length y then x else y

  let neutral = ""
end

module First4NaturalGroupTester = Group (First4NaturalsMul);;

First4NaturalGroupTester.check_identity
First4NaturalGroupTester.check_associativity
First4NaturalGroupTester.check_closure
First4NaturalGroupTester.check_invertibility

module MonoalphabeticStringTester = Group (MonoalphabeticString);;

MonoalphabeticStringTester.check_identity
MonoalphabeticStringTester.check_associativity
MonoalphabeticStringTester.check_closure
MonoalphabeticStringTester.check_invertibility

(* Test rings *)
module ZeroSet : RingOperation = struct
  type t = int

  let set = [0]

  let add = ( + )

  let mul = ( * )

  let neutral_add = 0

  let neutral_mul = 0
end

module ZeroRingTester = Ring (ZeroSet);;

ZeroRingTester.check_monoid_on_mul
ZeroRingTester.check_abelian_group_on_add
ZeroRingTester.check_distributive

module First4Natural : RingOperation = struct
  type t = int

  let set = [0; 1; 2; 3]

  let add a b = (a + b) mod 4

  let mul a b = a * b mod 4

  let neutral_add = 0

  let neutral_mul = 1
end

module First4NaturalRingTester = Ring (First4Natural);;

First4NaturalRingTester.check_monoid_on_mul
First4NaturalRingTester.check_abelian_group_on_add
First4NaturalRingTester.check_distributive

