(*
This exercise consists on an implementation of a simple social network via graph
data structure, whose nodes represent entities and edges relashionships between
entitities. 
In GraphADT.mli and Graph.ml you can find a definition of interface of the expected
operations on graph and the corresponding implementations. 

Here there are additional features, in particular an operation to visit a graph 7
starting from a specific node. For this scope I decided to implement breadth 
first search. Finally a simple test with a dummy social network.

To see results just open ocaml interpreter from terminal and type the following commands:
#load "GraphADT.mli";;
#load "Graph.ml";;
#load "social_network.ml";;
*)

open Graph

(* Build a graph given a list of couples that express the edges *)
let rec graph_of_list ?(g = Graph.empty ()) = function
  | [] -> g
  | (x, y) :: tl -> graph_of_list ~g:(Graph.add_arc x y g) tl

(* My own implementation of breadth first search algorithm *)
let bfs graph root =
  let rec inspect node adj queue arcs marked =
    match adj with
    | [] -> (queue, arcs, marked)
    | x :: tl when not (List.exists (fun y -> y = x) marked) ->
        inspect node tl (queue @ [x]) ((node, x) :: arcs) (x :: marked)
    | x :: tl -> inspect node tl queue arcs marked in
  let rec breadth queue arcs marked =
    match queue with
    | [] -> List.rev arcs
    | x :: tl ->
        let updated_queue, updated_arcs, updated_marked =
          inspect x (Graph.adjacents x graph) tl arcs marked in
        breadth updated_queue updated_arcs updated_marked in
  breadth [root] [] [root]

(* Type for tree representation after graph traversal *)
type 'a tree = Leaf of 'a | Tree of 'a * 'a tree list

let tree_from_traversed_graph g root =
  let rec explore node = function
    | [] -> Leaf node
    | adj -> Tree (node, create_tree adj)
  and create_tree = function
    | [] -> []
    | x :: tl -> explore x (Graph.adjacents x g) :: create_tree tl in
  explore root (Graph.adjacents root g)

(* A simple social network *)
let pl_social_network =
  [ ("Algol", "Pascal"); ("Algol", "C"); ("Algol", "Java"); ("C", "Java")
  ; ("Algol", "Python"); ("Pascal", "Modula 2"); ("C", "C++"); ("Java", "Scala")
  ; ("Lisp", "ML"); ("Lisp", "Scala"); ("Lisp", "Python"); ("Lisp", "Erlang")
  ; ("ML", "OCaML") ]

(* Some usage of implementend functionalities *)
let pl_graph = graph_of_list pl_social_network
let graph_from_bfs_on_Algol = graph_of_list (bfs pl_graph "Algol")
tree_from_traversed_graph graph_from_bfs_on_Algol "Algol"

let graph_from_bfs_on_Lisp = graph_of_list (bfs pl_graph "Lisp")
tree_from_traversed_graph graph_from_bfs_on_Lisp "Lisp"
