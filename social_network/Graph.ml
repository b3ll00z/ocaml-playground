module Graph : GraphADT.GraphADT = struct
  type 'a graph = Graph of 'a list * ('a * 'a) list

  exception TheGraphIsEmpty
  exception TheNodeIsNotInGraph

  let empty () = Graph ([], [])
  let is_empty = function Graph (nodes, _) -> nodes = []

  let rec add_element ?(res = []) k = function
    | [] -> List.rev (k :: res)
    | x :: tl as t when x = k -> res @ t
    | x :: tl -> add_element ~res:(x :: res) k tl

  let add_node n = function
    | Graph (nodes, edges) -> Graph (add_element n nodes, edges)

  let add_arc x y = function
    | Graph (nodes, edges) ->
        Graph (add_element y (add_element x nodes), add_element (x, y) edges)

  let node_is_in_graph node = function
    | Graph (nodes, _) -> List.exists (fun x -> x = node) nodes

  let adjacents n g =
    if not (node_is_in_graph n g) then raise TheNodeIsNotInGraph
    else
      match g with
      | Graph (_, edges) ->
          List.map snd (List.filter (fun x -> fst x = n) edges)
end
