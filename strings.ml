(* 
Define the following functions/operators on strings:
	is_palindrome: string -> bool 
	
	that checks if the string is palindrome,a string is palindrome when the 
	represented sentence can be read the same way in either directions in spite 
	of spaces, punctual and letter cases detatrated
*)
let remove_spurious_chars string_to_clean =
  let rec clear_string i =
    if i < String.length string_to_clean then
      if string_to_clean.[i] >= 'A' && string_to_clean.[i] <= 'Z' then
        Char.escaped string_to_clean.[i] ^ clear_string (i + 1)
      else clear_string (i + 1)
    else "" in
  clear_string 0

let is_palindrome string_to_check =
  let transformed_string =
    remove_spurious_chars (String.uppercase_ascii string_to_check) in
  let rec is_equal_chars = function
    | i, j when i < j ->
        if transformed_string.[i] == transformed_string.[j] then
          is_equal_chars (i + 1, j - 1)
        else false
    | i, j -> true in
  is_equal_chars (0, String.length transformed_string - 1)

is_palindrome "Do geese, see God?"
is_palindrome "Rise to vote, sir!"
is_palindrome "I'm not a palindrome string :P"

(*
operator (-): string → string → string that subtracts the letters in a string 
from the letters in another string,
 e.g., "Maccio Capatonda"-"abcwxyz" will give "Mio Cptnd" 
 note that the operator - is case sensitive
*)

let ( - ) source_string banned_chars =
  let rec extract_string i =
    if i < String.length source_string then
      if String.contains banned_chars source_string.[i] then
        extract_string (i + 1)
      else Char.escaped source_string.[i] ^ extract_string (i + 1)
    else "" in
  extract_string 0

"Maccio Capatonda" - "abcwxyz"
"Hildegarde  Scarlan" - "gl dan"

(*
anagram : string → string list → boolean that given a dictionary of strings, 
checks if the input string is an anagram of one or more of the 
strings in the dictionary
*)

let anagram source_string strings_dictionary =
  let to_list a_string =
    let rec extract_chars i =
      if i < String.length a_string then a_string.[i] :: extract_chars (i + 1)
      else [] in
    extract_chars 0 in
  let qsort_chars a_string =
    let rec q_sort = function
      | [] -> []
      | x :: xs ->
          let minors, majors = List.partition (fun y -> y < x) xs in
          q_sort minors @ [x] @ q_sort majors in
    q_sort (to_list a_string) in
  let sorted_clean_source_string =
    qsort_chars (remove_spurious_chars source_string) in
  List.exists
    (fun x ->
      qsort_chars (remove_spurious_chars x) == sorted_clean_source_string )
    strings_dictionary

let string_dict =
  [ "I'm a dot in place"; "Amen stories"; "an air miss"; "old England"
  ; "is a lane"; "runs a treat"; "care is noted"; "saintliness" ]

anagram "restaurant" string_dict
anagram "considerate" string_dict
anagram "there's no anagram :P" string_dict
