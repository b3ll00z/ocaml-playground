(*
sin(x) can be approximate by the Taylor's series:

Similarly you can approximate all the trigonometric and transcendent functions
(look at http://en.wikipedia.org/wiki/Taylor_series).

Let's write a module to implement sin x n by using the Taylor's series (where n
 is the level of approximation, i.e., 1 only one item, 2 two items, 3 three items
 and so on). Do the same with cosine, tangent, logarithm and so on.
*)
module TaylorSeries : sig
  val sin : float -> int -> float
  val cos : float -> int -> float
  val exp : float -> int -> float
  val ln : float -> int -> float
end = struct
  let rec fact n = if n > 0 then n * fact (n - 1) else 1
  let rec pow b e = if e > 0 then b *. pow b (e - 1) else 1.

  let sin x n =
    let rec sin_score acc i =
      if i <= n then
        let sin_cal =
          pow (-1.) i /. float (fact ((2 * i) + 1)) *. pow x ((2 * i) + 1) in
        sin_score (sin_cal +. acc) (i + 1)
      else acc in
    sin_score 0. 0

  let cos x n =
    let rec cos_score acc i =
      if i <= n then
        let cos_cal = pow (-1.) i /. float (fact (2 * i)) *. pow x (2 * i) in
        cos_score (cos_cal +. acc) (i + 1)
      else acc in
    cos_score 0. 0

  let exp x n =
    let rec exp_score acc i =
      if i <= n then exp_score (acc +. (pow x i /. float (fact i))) (i + 1)
      else acc in
    exp_score 0. 0

  let ln x n =
    let rec ln_score acc i =
      if i < n then
        let ln_acc = pow (-1.) (i - 1) *. pow (x -. 1.) i /. float i in
        ln_score (acc +. ln_acc) (i + 1)
      else acc in
    ln_score 0. 1
end

let angle = Float.pi /. 4.

TaylorSeries.sin angle 2
sin angle

TaylorSeries.sin angle 30
TaylorSeries.cos angle 2
cos angle

TaylorSeries.cos angle 30
TaylorSeries.exp 3. 3
exp 3.

TaylorSeries.exp 3. 30
TaylorSeries.ln 2. 3
log 2.
TaylorSeries.ln 2. 250
